Slides and subtitles for OpenSecurityTraining2 "Vulnerabilities 1002: C-Family Software Implementation Vulnerabilities" by [Xeno Kovah](https://twitter.com/XenoKovah) available at [https://ost2.fyi/Vulns1002](https://ost2.fyi/Vulns1002)

How to use the PPTX/PDF export scripts:  
From within the root of this folder, on a Mac, run:  
find . -type d -name "*_*" | xargs -n 1 -I {} ./keynote_export_script.sh -t pdf {}  
find . -type d -name "*_*" | xargs -n 1 -I {} ./keynote_export_script.sh -t pptx {}
